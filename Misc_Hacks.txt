Remove all your failed downloads from maven:

find ~/.m2  -name "*.lastUpdated" -exec grep -q "Could not transfer" {} \; -print -exec rm {} \;


For windows:
cd %userprofile%\.m2\repository
for /r %i in (*.lastUpdated) do del %i
Then rightclick on your project in eclipse and choose Maven->"Update Project ...", make sure "Update Dependencies" is checked in the resulting dialog and click OK.